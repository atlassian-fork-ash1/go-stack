# The Atlassian Go Stack (EXPERIMENTAL, DO NOT USE IN PRODUCTION)

This project serves to abstract the tooling api from their implementations.
At the simplest level this repo performs namespace hiding,
the cost of which will be trivially erased during compile time optimization.

Stack consumers will be insulated from changes in the preferred library,
any version upgrades to downstream packages will be vetted end to end for:
 * performance impact
 * potential security vulnerabilities
 * transitive dependency cost
 * build cost


## Creating a project from template

```bash
go get bitbucket.org/atlassian/go-stack
go install bitbucket.org/atlassian/go-stack/create-project/create-go-project.go
create-go-project <atlassian-project> <service-name>
cd bitbucket.org/atlassian/<service-name>
git add -A
git status
```
