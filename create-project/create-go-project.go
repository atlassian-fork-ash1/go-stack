package main

import (
	"errors"
	"flag"
	"fmt"
	"gopkg.in/src-d/go-billy.v4/osfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing/cache"
	"gopkg.in/src-d/go-git.v4/storage/filesystem"
	"html/template"
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strconv"
	"strings"
)

type name struct {
	Dash  string
	Block string
}

type db struct {
	Port int
}

type service struct {
	Owner   string
	Project string
	Name    name
	Desc    string
	DB      db
}

type ServiceDetails struct {
	Service service
}

func username() (string, error) {
	var username string
	if user, err := user.Current(); err == nil {
		username = user.Username
	} else {
		username = os.Getenv("USER")
	}
	if username == "" {
		return "", errors.New("failed to get username")
	}
	return username, nil
}

func exists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func ReadFileTree(dirname string) (chan string) {
	c := make(chan string)
	get := func(dirname string) ([]os.FileInfo, error) {
		f, err := os.Open(dirname)
		if err != nil {
			return nil, err
		}
		list, err := f.Readdir(-1)
		f.Close()
		if err != nil {
			return nil, err
		}
		return list, nil
	}
	var recurs func(string)
	recurs = func(dirname string) {
		slice, _ := get(dirname)
		for _, info := range slice {
			path := dirname + "/" + info.Name()
			stat, _ := os.Stat(path)
			if (stat.IsDir()) {
				recurs(path)
			} else {
				c <- path
			}

		}
	}
	go func() { recurs(dirname); close(c) }()
	return c
}

func main() {
	flag.Parse()
	if flag.NArg() != 2 {
		fmt.Println("Useage: ")
		fmt.Println("\tcreate-go-project <name-of-project> <name-of-service>")
		fmt.Println("Number of args present: " + strconv.Itoa(flag.NArg()))
		return
	}
	var err error
	replaceWithDashes := strings.NewReplacer("_", "-", " ", "-", ".", "-")
	projectName := strings.ToLower(replaceWithDashes.Replace(flag.Arg(0)))
	serviceName := strings.ToLower(replaceWithDashes.Replace(flag.Arg(1)))
	blockName := strings.ToUpper(strings.Replace(serviceName, "-", "_", -1))

	u, err := username()
	details := ServiceDetails{
		Service: service{
			Owner:   u,
			Project: projectName,
			Name: name{
				Dash:  serviceName,
				Block: blockName,
			},
			Desc: "",
			DB: db{
				Port: 5432,
			},
		},
	}

	// go get bitbucket.org/atlassian/go-stack-template
	// mkdir $GOPATH/src/bitbucket.org/atlassian/name-of-service
	// for all files in $GOPATH/src/bitbucket.org/atlassian/go-stack-template
	//    make template from file
	//    create file under $GOPATH/src/bitbucket.org/atlassian/name-of-service
	//
	var repo *git.Repository
	goPath := os.Getenv("GOPATH")
	templateRoot := goPath + "/src/bitbucket.org/atlassian/go-stack-template"
	projectRoot := goPath + "/src/bitbucket.org/atlassian/" + serviceName

	fmt.Println("Checking for template repo at " + templateRoot)
	if exists(templateRoot + "/.git") {
		fmt.Println("Attempting to pull " + templateRoot)
		repo, err = git.PlainOpen(templateRoot)

		w, err := repo.Worktree()
		if err != nil {
			fmt.Println(err)
		} else {
			err = w.Pull(&git.PullOptions{
				RemoteName:   "origin",
				Progress:     os.Stdout,
				SingleBranch: true,
			})
			if err != nil {
				fmt.Println(err)
			}
		}
	} else {
		fmt.Println("Attempting to clone into " + templateRoot)
		repo, err = git.PlainClone(templateRoot, false, &git.CloneOptions{
			URL:          "git@bitbucket.org:atlassian/go-stack-template.git",
			Progress:     os.Stdout,
			SingleBranch: true,
		})
		if err != nil {
			panic(err)
		}
	}

	// template-copy
	fmt.Println("Creating from files: ")
	for p := range ReadFileTree(templateRoot) {
		if !strings.Contains(p, ".gitignore") && strings.Contains(p, "/.") {
			continue
		}
		target := strings.Replace(p, templateRoot, projectRoot, 1)
		fmt.Println("\t" + target)
		t := template.New(p)
		contents, err := ioutil.ReadFile(p)
		if err != nil {
			fmt.Println(err)
			continue
		}
		t, err = t.Parse(string(contents))
		if err != nil {
			fmt.Println(err)
			continue
		}
		os.MkdirAll(path.Dir(target), 0755)
		w, err := os.OpenFile(target, os.O_WRONLY|os.O_CREATE, 0644)
		if err == nil {
			err = t.Execute(w, &details)
			if err != nil {
				fmt.Println(err)
			}
		} else {
			fmt.Println(err)
		}
		w.Close()
	}

	// git init
	if !exists(templateRoot + "/.git") {
		wt := osfs.New(projectRoot)
		store := filesystem.NewStorage(
			osfs.New(projectRoot+"/.git"),
			cache.NewObjectLRUDefault(),
		)
		if err != nil {
			panic(err)
		}
		projRepo, err := git.Init(store, wt)
		if err != nil {
			panic(err)
		}
		_, err = projRepo.CreateRemote(&config.RemoteConfig{
			Name: "origin",
			URLs: []string{"git@bitbucket.org:atlassian/" + serviceName + ".git"},
		})
		if err != nil {
			panic(err)
		}
	}
}
