package jwt

import (
	"testing"
	"net/http"
	"bytes"
	"github.com/dgrijalva/jwt-go"
	"time"
	"net/http/httptest"
	"reflect"
)

func Test_ObjectIsRecoverable(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		claims := GetClaims(r)
		stdClaims, ok := claims.(*jwt.StandardClaims)
		if !ok {
			t.Error(reflect.TypeOf(claims).String())
		}
		expAt := time.Unix(stdClaims.ExpiresAt, 0)
		if expAt.Before(time.Now()) {
			t.Error(expAt.String())
		}
		w.WriteHeader(200)
	})

	stdClaim := jwt.StandardClaims{
		ExpiresAt: time.Now().Add(24 * time.Hour).UTC().Unix(),
	}
	secretKey := "21a42d88-9bd7-4ecb-8cb1-9ba8f350d5ff"
	req, err := http.NewRequest("POST", "https://localhost/path/to/file", &bytes.Buffer{})
	if err != nil {
		t.Error(err)
	}
	RequestWithSignedObject(req, []byte(secretKey), stdClaim)
	claimFac := func() Claims {
		return &jwt.StandardClaims{}
	}
	middleware := RequireJwtToken(secretKey, claimFac)
	servlet := middleware(handler)

	resp := httptest.NewRecorder()
	servlet.ServeHTTP(resp, req)

	if resp.Code != 200 {
		t.Fail()
	}
}
