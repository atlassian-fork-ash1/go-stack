Current implementation wraps (chi)[https://github.com/go-chi/chi]


Note that while chi's patterns do support regex, you are advised to limit this to simple expressions
such as alpha/alpha-numeric/numeric-only.

It is also advised that all alpha params be case insensitive, such that functionality would not be
effected as, for example, the result of router.StringParam(r, "name") returned strictly lowercase values.


```go
import (
	"bitbucket.org/atlassian/go-stack/router"
	"bitbucket.org/atlassian/go-stack/log"
	"net/http"
	"io"
)

var LOG, _ = log.New("examples")

func accessLogger(f http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer LOG.Sync()
		LOG.Info(
			"access",
			log.String("method", r.Method),
			log.String("path", r.URL.Path),
			log.String("ip", r.RemoteAddr),
		)
		f.ServeHTTP(w, r)
	})
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	name := router.StringParam(r, "name")
	io.WriteString(w, "Hello "+name)
}

func main() {
	r := router.New()
	r.Use(accessLogger)
	r.Route(
		"/hello",
		func(r router.Router) {
			r.Get("/world/{name:[a-zA-Z]+}", helloWorld)
		},
	)
	http.ListenAndServe(":3000", r)
}
```