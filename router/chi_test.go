package router

import (
	"testing"
	"net/http"
	"net/http/httptest"
	"bytes"
	"fmt"
)

func Test_ChiRoute(t *testing.T) {
	router := New()
	router.Route("/path", func (r Router){
		r.Get("/to/file", func(w http.ResponseWriter, r *http.Request){
			panic("Trigger test recover code")
		})
	})
	r, _ := http.NewRequest("GET", "http://localhost/path/to/file", &bytes.Buffer{})
	w := httptest.NewRecorder()
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("recovering")
			return
		}
	}()
	router.ServeHTTP(w, r)
	t.Error("No panic triggered.")
}