//go:generate mockgen -source=types.go -destination=./mocks/logger.go -package=mock_stack Logger

package gostack

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"net/http"
)

type Middleware = func(handler http.Handler) http.Handler

type Field = zapcore.Field
type Option = zap.Option

type Logger interface {
	Info(msg string, fields ...Field)
	Debug(msg string, fields ...Field)
	Sync() error
}